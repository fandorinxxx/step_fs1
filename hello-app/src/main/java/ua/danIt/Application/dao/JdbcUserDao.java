package ua.danIt.Application.dao;

import static java.sql.DriverManager.getConnection;
import static ua.danIt.framework.LoginServlet.curID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import ua.danIt.Application.model.User;
import ua.danIt.framework.utils.EntityUtils;

public class JdbcUserDao implements UserDao{
  String PASSWD = "paRis";
  String DB_URL = "jdbc:mysql://danit.cukm9c6zpjo8.us-west-2.rds.amazonaws.com:3306/fs1";
  String USER = "fs1_user";

  @Override
  public List<User> getAllUsers(int id) {
    String sql ="SELECT *FROM Users WHERE ID!='"+id+"'";
   return EntityUtils.nativeQuery(sql,User.class);
  }

  @Override
  public List<User> getSelectedUsers() {
    String sql ="SELECT *FROM Users WHERE ID!='"+curID+"'";
    return EntityUtils.nativeQuery(sql,User.class);
  }

  @Override
  public void selectUser(Integer userId) throws SQLException {
    String sql = "INSERT INTO `Matches`(who,whom) VALUES (?,?)";
    try(Connection con = getConnection(DB_URL, USER, PASSWD);
        PreparedStatement ps = con.prepareStatement
            (sql)) {
      ps.setInt(1, curID);
      ps.setInt(2, userId);
      ps.executeUpdate();
    }
  }

  @Override
  public User getCurrentUser() {
   String sql = "SELECT * FROM Users WHERE ID!='"+curID+"' AND ID NOT IN (SELECT whom FROM Matches where whom<>'"+curID+"')";
    List<User> users = EntityUtils.nativeQuery(sql, User.class);
    return users.iterator().next();
  }

  public List<User> getUserLoginPassword(String login, String password) {
    String sql = "select * from fs1.Users where `LOGIN`='" + login + "' and `PASSWORD`='" + password + "'";
    return EntityUtils.nativeQuery(sql, User.class);
  }

  public User getUserByID(int id) throws SQLException {
    String sql = "select * from Users where ID='"+id+"'";
     return EntityUtils.nativeQuery(sql,User.class).get(0);
  }
}
