package ua.danIt.framework;

import static ua.danIt.framework.LoginServlet.curID;
import static ua.danIt.framework.LoginServlet.yes;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.danIt.Application.dao.JdbcMessageDao;
import ua.danIt.Application.dao.JdbcUserDao;
import ua.danIt.Application.model.Message;

public class ChatRoomServlet extends HttpServlet {
  public  static String head = "<!doctype html>\n" +
      "<html lang=\"en\" xmlns:margin-bottom=\"http://www.w3.org/1999/xhtml\">\n" +
      "<head>\n" +
      "    <meta charset=\"UTF-8\">\n" +
      "    <meta NAME=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
      "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
      "    <title>Document</title>\n" +
      "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">";


  public static String style = "<style>\n" +
      "        .container {\n" +
      "    border: 2px solid #dedede;\n" +
      "    background-color: #f1f1f1;\n" +
      "    border-radius: 5px;\n" +
      "    padding: 10px;\n" +
      "    margin: 10px 0;\n" +
      "}\n" +
      "\n" +
      "/* Darker chat container */\n" +
      ".darker {\n" +
      "    border-color: #ccc;\n" +
      "    background-color: #ddd;\n" +
      "}\n" +
      "\n" +
      "/* Clear floats */\n" +
      ".container::after {\n" +
      "    content: \"\";\n" +
      "    clear: both;\n" +
      "    display: table;\n" +
      "}\n" +
      "\n" +
      "/* Style images */\n" +
      ".container img {\n" +
      "    float: left;\n" +
      "    max-width: 60px;\n" +
      "    width: 100%;\n" +
      "    margin-right: 20px;\n" +
      "    border-radius: 50%;\n" +
      "}\n" +
      "\n" +
      "/* Style the right image */\n" +
      ".container img.right {\n" +
      "    float: right;\n" +
      "    margin-left: 20px;\n" +
      "    margin-right:0;\n" +
      "}\n" +
      "\n" +
      "/* Style TIMESTAMP MSG_TEXT */\n" +
      ".TIMESTAMP-right {\n" +
      "    float: right;\n" +
      "    color: #aaa;\n" +
      "}\n" +
      "\n" +
      "/* Style TIMESTAMP MSG_TEXT */\n" +
      ".TIMESTAMP-left {\n" +
      "    float: left;\n" +
      "    color: #999;\n" +
      "}\n" +
      "    </style>";
  JdbcMessageDao messageDao = new JdbcMessageDao();
  JdbcUserDao userDao = new JdbcUserDao();
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
  if (yes){
    PrintWriter out = resp.getWriter();
    out.write(head);
    out.write(style);
    out.write("</head>");
    out.write("<body>");
    int user = Integer.parseInt(req.getPathInfo().substring(1));
    try {
      out.write("<h1>"+ userDao.getUserByID(user).NAME+"</h1>");
    } catch (SQLException e) {
      e.printStackTrace();
    }
    for (Message msg: messageDao.showUsers(user)){
      if (msg.FROM_USER ==user){
        out.write("<div class=\"container\">");
        out.write("<p>"+msg.MSG_TEXT +"</p>");
        out.write("</div>");

      }else {
        out.write("<div class=\"container darker\">");
        out.write("<p>"+msg.MSG_TEXT +"</p>");
        out.write("</div>");
      }
    }

    out.write("<form action='' method='POST'>");
    out.write("<input class=\"col-sm-offset-1 col-sm-4 col-sm-offset-4 \" NAME='todoText' type='MSG_TEXT'>");
    out.write("<input NAME='ID' type='hidden' value ="+Integer.parseInt(req.getPathInfo().substring(1))+" >");
    out.write("<button class=\"btn col-sm-offset-1 col-sm-2 btn-primary \" type='submit'>Submit</button>");
    out.write("</form>");

    out.write("</body></html>");
  } else {
    resp.sendRedirect("/login");
  }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String text=req.getParameter("todoText");
    String id = req.getParameter("ID");
    int idTo = Integer.parseInt(id);
    try {
      messageDao.save(new Message(curID,idTo,text, System.currentTimeMillis()));
    } catch (SQLException e) {
      e.printStackTrace();
    }
    resp.sendRedirect("/message/"+idTo);
  }
}
