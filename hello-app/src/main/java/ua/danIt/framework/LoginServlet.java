package ua.danIt.framework;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.danIt.Application.dao.JdbcUserDao;
import ua.danIt.Application.model.User;


public class LoginServlet extends HttpServlet {
JdbcUserDao userDao = new JdbcUserDao();
public static int curID;
public static boolean yes = false;
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    PrintWriter out = resp.getWriter();
    out.write("<html><body>");
    out.write("  <form action='/login' method='POST'>");
    out.write("    <label>User Name</label>");
    out.write("    <input name='userName' type='text'><br>");

    out.write("    <label>Password</label>");
    out.write("    <input name='password' type='password'><br>");

    out.write("    <button type='submit'>Login</button>");
    out.write("  </form>");
    out.write("</body></html>");
  }

  public static Map<String, String> tokens = new HashMap<>();

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String userName = req.getParameter("userName");
    String password = req.getParameter("password");

    for (User user: userDao.getUserLoginPassword(userName,password)){
      if (user.LOGIN.equals(userName)&& user.PASSWORD.equals(password)){
        yes=true;
        curID= user.ID;
        String token = UUID.randomUUID().toString();
        tokens.put(userName,token);
        resp.addCookie(new Cookie("user-token", token));
      }
    }
    try {
      if (yes==true && tokens.containsKey(new JdbcUserDao().getUserByID(curID).LOGIN)){
        resp.sendRedirect("/");
      }else {
        resp.getWriter().write(userName+"I don't know you.");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}